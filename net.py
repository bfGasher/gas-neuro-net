import random
import sys

number_size = 15

class Number:
  def __init__(self, value):
    self.value = value
    self.items = None

def read_numbers(filename):
  numbers = []
  file = open(filename, 'r')
  lines = file.read().split('\n')
  for line in lines :
    numbers.append(line)
  file.close()
  return numbers

# Является ли данное число 5
def proceed(number, weights):
  bias = 7 # порог функции активации
  net = 0 # Рассчитываем взвешенную сумму
  for i in range(number_size):
    net += int(number[i]) * int(weights[i])
    # Превышен ли порог? 
    # (Да - сеть думает, что это 5. 
    # Нет - сеть думает, что это другая цифра)
  return net >= bias

# Уменьшение значений весов, если сеть ошиблась и выдала 1
def decrease(number, weights):
  for i in range(number_size):
    # Возбужденный ли вход
    if int(number[i]) == 1:
      # Уменьшаем связанный с ним вес на единицу
      weights[i] -= 1

# Увеличение значений весов, если сеть ошиблась и выдала 0
def increase(number, weights):
  for i in range(number_size):
    # Возбужденный ли вход
    if int(number[i]) == 1:
      # Увеличиваем связанный с ним вес на единицу
      weights[i] += 1

def educate(weights, iterations, nums, num, num_int):
  # Тренировка сети
  for i in range(iterations):
    # Генерируем случайное число от 0 до 9
    option = random.randint(0, 9)
 
    # Если получилось НЕ число 5
    if option != num_int:
      # Если сеть выдала True/Да/1, то наказываем ее
      if proceed(nums[option], weights):
        decrease(nums[option], weights)
        # Если получилось число 5
      else:
        # Если сеть выдала False/Нет/0, то показываем, что эта цифра - то, что нам нужно
        if not proceed(num, weights):
          increase(num, weights)

def display_and_test(weights, numbers,  current):
  print(weights,'\n')

  # Вывод значений весов
  cop1 = weights[:]
  cop2 = cop1[:]

  for i in range(5):
    print(('{:>2}'*3).format(*cop1))
    cop1 = cop1[3:]

  for i in range(number_size):
    if cop2[i]>0:
      cop2[i]='+'
    else:
      cop2[i]='.'
  print()

  for i in range(5):
    print(('{:>2}'*3).format(*cop2))
    cop2 = cop2[3:]
  print()

  # прогон по обучающей выборке
  i = 0
  for num in numbers :
    print('{0} это {1}? '.format(i, proceed(num, weights)))
    i += 1
  print()

  # прогон по тестовой выборке
  i = 0
  print('Узнал {}? - {}'.format(current[0],proceed(current[1], weights) ))
  for num in numbers :
    print('Узнал {} - {}?'.format(i, proceed(num, weights)))
    i += 1
  print()

def select_number():
  number = -1
  while (number < 0) and (number > 9) :
    number = int(input())
  return number


def read_options(filename):
  file = open(filename, 'r')
  lines = file.read().split('\n')
  options = {}
  for line in lines:  
    comment_index = line.find('#')
    if comment_index >= 0 :
      line = line[comment_index : len(line) - 1]
    pair = line.split('=')
    if len(pair) > 1:
      key = pair[0].strip()
      value = pair[1].strip()
      if (options.get(key) == None):
        options[key] = value
  file.close()
  return options

def title():
  f = open('readme.txt')
  print(f.read())
  f.close()

def main():
  title()
  # init weight's
  weights = [0 for i in range(number_size)]
  print('Reading options...')
  # get options filename from arguments.
  options_filename = 'options.txt'
  if len(sys.argv) > 1 : 
    options_filename = sys.argv[1]

  options = read_options(options_filename)
  print('Options loaded.')
  print('Reading data...')
  template_numbers = read_numbers(options['numbers_filename'])
  test_numbers = read_numbers(options['test_filename'])
  current_number = int(options['current_number'])
  print('Data setup successfull. Start!')
  print('Education. Iterations count = {}'.format(int(options['iterations'])))
  educate(weights, int(options['iterations']), template_numbers, template_numbers[current_number], current_number)
  print('Run testing samples.')
  display_and_test(weights, template_numbers, (current_number, template_numbers[current_number]) )

main()